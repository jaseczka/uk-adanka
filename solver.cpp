#include "solver.hpp"
#include "board.hpp"
#include "astarnode.hpp"
#include <QScopedPointer>
#include <queue>
#include <set>

#include <iostream>

Solver::Solver()
{
}


int Solver::distance(const Board& board)
{
    int sum = 0;
    for (int i = 1; i < Board::SIZE; i++) {
        sum += board.distance(board.index(i), i - 1);
    }
    return sum;
}


bool delete_worse(std::set<AStarNode*> s, AStarNode* n)
{
    std::set<AStarNode*>::iterator it = s.find(n);
    if (it != s.end()) {
        if ((*it)->cost() <= n->cost()) {
            delete n;
            n = NULL;
            return true;
        }
        //usuwam z seta stary wierzchołek, żeby można było wstawić nowy
        //w kolejce musi zostać stary
        //bo się nie da usuwać ze środka stlowej priority_queue
        s.erase(it);
    }
    return false;
}


void Solver::push_one(AStarQueue& q, std::set<AStarNode*>& opened,
                      std::set<AStarNode*>& closed, const AStarNode* node,
                      const Board::direction d)
{
//    std::cout << "PUSHONE" << std::endl;
    Board* new_board = new Board(*node->board);
    if (new_board->move_empty(d)) {
//        std::cout << "ruch z " << node->board->empty_pos() << " na " << new_board->empty_pos() << std::endl;
        int depth = node->depth + 1;
        int heuristic = distance(*new_board);
        AStarNode* one = new AStarNode(new_board, depth, heuristic, node, d);

        if (delete_worse(opened, one)) return;
        if (delete_worse(closed, one)) return;

        opened.insert(one);
        q.push(one);
    }

}

void Solver::push_neighbours(AStarQueue& q, std::set<AStarNode*>& opened,
                             std::set<AStarNode*>& closed, const AStarNode* node)
{
    push_one(q, opened, closed, node, Board::UP);
    push_one(q, opened, closed, node, Board::DOWN);
    push_one(q, opened, closed, node, Board::RIGHT);
    push_one(q, opened, closed, node, Board::LEFT);
}

std::deque<Board::direction>* Solver::solve(const Board &board)
{
    AStarQueue opened_queue;
    std::set<AStarNode*> opened;
    std::set<AStarNode*> closed;
    AStarNode* start = new AStarNode(&board, 0, distance(board), NULL, Board::direction(0));
    AStarNode* current = start;
    AStarNode* goal = new AStarNode(new Board(), 0, 0, NULL, Board::direction(0));
    opened_queue.push(start);
    opened.insert(start);

    int count = 0;
    while (*goal != *current) {
        current = opened_queue.top();
        count++;
//        std::cout << count << " d: " << current->depth << " h: " << current->heuristic << "\n";
//        std::cout << "rozmiar zbioru: " << opened.size() << std::endl;
//        std::cout << "PLANSZA SOLVE\n";
//        current->board->write();
//        std::cout << "dostepne koszty: ";
        std::set<AStarNode*>::iterator it;
//        for (it = opened.begin(); it != opened.end(); ++it) {
//            std::cout << (*it)->cost() << " ";
//        }
//        std::cout << std::endl;

        opened_queue.pop();
        opened.erase(current);

        closed.insert(current);
        push_neighbours(opened_queue, opened, closed, current);
    }
    std::cout << "count: " << count << std::endl;

    std::deque<Board::direction>* moves = new std::deque<Board::direction>();
    //current wskazuje na cel
    while (*current != *start) {
        moves->push_front(current->direction);
        *current = *current->parent;
    }
    std::cout << "rozwiazanie jest dlugosci: " << moves->size() << std::endl;
    return moves;
}
