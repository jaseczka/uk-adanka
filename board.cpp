#include<vector>
#include<cmath>
#include<QScopedPointer>

#include "board.hpp"

#include <iostream>

Board::Board() : board(0)
{
	for(int i = 0; i < SIZE - 1; i++) {
		put(i + 1, i);
	}
	put(0, SIZE - 1);
}

Board::Board(const std::vector<int>& v) : board(0)
{
	for(int i = 0; i < SIZE; i++) {
		put(v[i], i);
	}
}

void Board::put(const int what, const int where)
{
    unsigned long long what_long = (unsigned long long) what;
	what_long = what_long << (where*4);
	unsigned long long mask = ~(mask_1111 << (where*4));
	board = (board & mask) | what_long;
}

int Board::at(int index) const
{
    return (int) ((board >> (index*4)) & mask_1111);
}

int Board::index(const int n) const
{
    for (int i = 0; i < SIZE; i++) {
        if (at(i) == n)
            return i;
    }
    return -1;
}

int Board::empty_pos() const
{
    return index(0);
}

std::vector<int>* Board::vector() const
{
    std::vector<int>* v = new std::vector<int>(SIZE);
    for(int i = 0; i < SIZE; i++) {
		v->at(i) = at(i);
	}
	return v;
}


constexpr int Board::SHIFT[4];

int Board::shifted(const int index, const direction d) const
{
	if (d == UP && index < ROW_LENGTH)
		return -1;
	if (d == DOWN && index >= (ROW_LENGTH - 1) * ROW_LENGTH)
		return -1;
	if (d == LEFT && index % ROW_LENGTH == 0)
		return -1;
	if (d == RIGHT && index % ROW_LENGTH == ROW_LENGTH - 1)
		return -1;
    return index + SHIFT[d];
}

bool Board::move(int index)
{
    int i = -1;
    direction d = direction(i);
	int neighbour;
	bool found = false;
	do {
        i++;
        d = direction(i);
        neighbour = shifted(index, d);
		found = neighbour >= 0 && empty(neighbour);
    } while (!found && d < LAST_DIR);
	if (found) {
		swap(index, neighbour);
		return true;
	}
    return false;
}

bool Board::move_empty(const direction d)
{
    int empty = empty_pos();
    int neighbour = shifted(empty, d);
    if (neighbour >= 0) {
        swap(empty, neighbour);
        return true;
    }
    return false;
}

void Board::swap(const int first, const int second)
{
	int val_first = at(first);
	int val_second = at(second);
	put(val_first, second);
    put(val_second, first);
}

bool Board::empty(int index) const
{
    if (index < 0)
        return false;
    return at(index) == 0;
}

bool Board::ordered() const {
    for (int i = 1; i < SIZE - 1; i++)
        if (at(i - 1) > at(i))
            return false;
    return true;
}

int Board::row(const int index) const
{
    return index / ROW_LENGTH;
}

int Board::col(const int index) const
{
    return index % ROW_LENGTH;
}

int diff(int lhs, int rhs)
{
    return lhs > rhs ? lhs - rhs : rhs - lhs;
}

int Board::distance(const int ind1, const int ind2) const
{
    return diff(row(ind1), row(ind2)) +
            diff(col(ind1), col(ind2));
}

bool operator== (const Board& lhs, const Board& rhs)
{
    return lhs.board == rhs.board;
}

void Board::write() const
{
    std::cout << "====\n";
    std::cout << at(0) << "  " << at(1) << "  " << at(2) << "  " << at(3) << std::endl;
    std::cout << at(4) << "  " << at(5) << "  " << at(6) << "  " << at(7) << std::endl;
    std::cout << at(8) << "  " << at(9) << "  " << at(10) << "  " << at(11) << std::endl;
    std::cout << at(12) << "  " << at(13) << "  " << at(14) << "  " << at(15) << std::endl;
    std::cout << "====\n";
}
