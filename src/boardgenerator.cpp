#include <algorithm>
#include <vector>
#include "boardgenerator.hpp"
#include "board.hpp"

#include <iostream>

BoardGenerator::BoardGenerator()
{
}

Board BoardGenerator::random()
{
    std::vector<int> v(Board::SIZE);
    for (int i = 1; i < Board::SIZE; i++) {
        v[i-1] = i;
	}
    random_shuffle(v.begin(), v.end()-1);
    if (!proper(v)) {
        int tmp = v[0];
        v[0] = v[1];
        v[1] = tmp;
    }

    //czy zła permutacja nie będzie ok, jeśli zamienię dowolne 2 el?
    v[Board::SIZE - 1] = 0;
    Board board = Board(v);
	return board;
}

Board BoardGenerator::random(int n)
{
    Board board = Board();
    int i = 0;
    while (i < n) {
        Board::direction d = Board::direction(rand() % 4);
        if (board.move_empty(d)) {
            i++;
        }
    }
    return board;
}

bool BoardGenerator::proper(std::vector<int> v) const
{
    int begin = 1;
    int count = 0;
    int cycle_len;
    int next1, next2;
    //sprawdzam pozycje 0 - 14
    while (begin < Board::SIZE) {
        if (v[begin-1] >= 0) {
            cycle_len = 1;
            next1 = v[begin-1];
            v[begin-1] = -1;
            while (next1 != begin) {
                next2 = next1;
                next1 = v[next1-1];
                v[next2-1] = -1;
                cycle_len++;
            }
            if (cycle_len % 2 == 0)
                count++;
        }

        begin++;
    }

    return count % 2 == 0;
}
