# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/astarnode.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/astarnode.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/board.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/board.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/boardgenerator.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/boardgenerator.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/game.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/game.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/main.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/main.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/mainwindow.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/mainwindow.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/moc_mainwindow.cxx" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/moc_mainwindow.cxx.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/moc_tile.cxx" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/moc_tile.cxx.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/moc_tilemanager.cxx" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/moc_tilemanager.cxx.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/solver.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/solver.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/tile.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/tile.cpp.o"
  "/home/students/inf/m/mt266699/Repos/uk-adanka/src/tilemanager.cpp" "/home/students/inf/m/mt266699/Repos/uk-adanka/src/CMakeFiles/GameOfFifteen.dir/tilemanager.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "QT_GUI_LIB"
  "QT_CORE_LIB"
  "QT_DEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  "src"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
