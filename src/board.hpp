#ifndef BOARD_HPP
#define BOARD_HPP

#include <vector>

class Board
{
public:
    enum direction
    {
        UP = 0,
        DOWN = 1,
        LEFT = 2,
        RIGHT = 3
    };
    const static int ROW_LENGTH = 4;
    const static int SIZE = ROW_LENGTH * ROW_LENGTH;
    //domyślny konstruktor daje ułożoną planszę
    Board();
    //tworzy planszę na podstawie wektora permutacji
    Board(const std::vector<int>&);
    //zwraca wskaźnik do planszy w postaci wektora
    std::vector<int>* vector() const;
    //numer klocka na pozycji
    int at(const int index) const;
    //indeks klocka o danym numerze
    int index(const int n) const;
	//podaj pustą pozycję
    int empty_pos() const;
    //czy dane miejsce jest puste
    bool empty(const int index) const;
    //przesuwa płytkę z danej pozycji na puste miejsce,
    //jeśli płytka jest obok pustego miejsa
    //zwraca true jeśli się udało
    bool move(const int index);
    bool move_empty(const direction d);
    //czy plansza jest ułożona
    bool ordered() const;
    int row(const int index) const;
    int col(const int index) const;
    int distance(const int ind1, const int ind2) const;
    friend bool operator== (const Board& lhs, const Board& rhs);
    void write() const;
private:
    unsigned long long board;
    const static unsigned long long mask_1111 = 15;
    constexpr static int SHIFT[4] = {-ROW_LENGTH, ROW_LENGTH, -1, 1};
    const static direction FIRST_DIR = UP, LAST_DIR = RIGHT;
    //zwraca pozycję sąsiedniego elementu lub -1 jeśli nie ma
    int shifted(const int index, const direction d) const;
    void put(const int what, const int where);
    void swap(const int first, const int second);
};

#endif // BOARD_HPP
