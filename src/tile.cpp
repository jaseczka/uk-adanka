#include "tile.hpp"
#include "tilemanager.hpp"

#include <QPainter>
#include <QMessageBox>

Tile::Tile(int id): id(id)
{
    QPixmap image;
    image.load("rysunek.png");

    int imageXsize = image.width()/4;
    int imageYsize = image.height()/4;

    int imgX = (id%4)*imageXsize;
    int imgY = (id/4)*imageYsize;

    setPixmap(image.copy( imgX, imgY, imageXsize, imageYsize));
}

void Tile::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    emit clicked(id);
}

int Tile::get_id()
{
    return id;
}
