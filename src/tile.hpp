#ifndef TILE_HPP
#define TILE_HPP

#include <QGraphicsWidget>
#include <QObject>

class Tile : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
    Tile(int id);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    int get_id();

private:
    int id;

signals:
    void clicked(int);
 };

#endif // TILE_HPP
