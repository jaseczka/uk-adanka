#include "astarnode.hpp"
#include <QScopedPointer>

AStarNode::AStarNode(): board(NULL), depth(0), heuristic(0),
    parent(NULL), direction(Board::direction(0))
{
}

AStarNode::AStarNode(const Board* board, int depth,
                     int heuristic, const AStarNode* parent,
                     Board::direction direction):
    board(board), depth(depth), heuristic(heuristic),
    parent(parent), direction(direction)
{
}

int AStarNode::cost() const
{
    return depth + 4 * heuristic;
}

AStarNode::~AStarNode()
{
    delete board;
    board = NULL;
}

bool AStarNode::operator< (const AStarNode &other ) const
{
    return this->cost() > other.cost();
}

bool AStarNode::operator> (const AStarNode &other) const
{
   return this->cost() < other.cost();
}


bool AStarNode::operator== (const AStarNode &other ) const
{
    return *this->board == *other.board;
}

bool AStarNode::operator!= (const AStarNode &other ) const
{
    return !(*this == other);
}
