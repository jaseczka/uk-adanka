#ifndef ASTARNODE_HPP
#define ASTARNODE_HPP

#include "board.hpp"

class AStarNode
{
public:
    AStarNode();
    AStarNode(const Board* board, int depth, int heuristic, const AStarNode* parent, Board::direction direction);
    const Board* board;
    int cost() const;
    int depth;
    int heuristic;
    const AStarNode* parent;
    Board::direction direction;
    bool operator< (const AStarNode &other ) const;
    bool operator> (const AStarNode &other ) const;
    bool operator== (const AStarNode &other ) const;
    bool operator!= (const AStarNode &other ) const;
    //zwalnia pamięć planszy, rodzica trzeba zwolnić samemu
    virtual ~AStarNode();
};


#endif // ASTARNODE_HPP
