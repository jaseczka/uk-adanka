#include <QtGui/QApplication>
#include "mainwindow.h"

#include <ctime>
#include "game.hpp"
#include "board.hpp"
#include <iostream>
#include "boardgenerator.hpp"
#include "astarnode.hpp"

#undef main

int main(int argc, char* argv[])
{
    srand(time(NULL));

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();

//debagowanie
//    Game g = Game(100);

//    g.get_board().write();


//    g.solve();





//    return 0;
}
