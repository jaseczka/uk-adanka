#include "tilemanager.hpp"
#include "moc_tilemanager.cpp"
#include "game.hpp"

TileManager::TileManager(QObject* parent, Game* game) : QGraphicsScene(parent), game(game)
{

}

void TileManager::setup()
{
    tiles.resize(16);
    for(int i=0; i<16; ++i) {
        tiles[i] = new Tile(game->at(i));
        this->addItem(tiles[i]);

        connect(tiles[i], SIGNAL(clicked(int)), this, SLOT(click(int)));

        int x = 0.25*(i%4)*width();
        int y = 0.25*(i/4)*height();
        tiles[i]->setPos(x, y);
    }

}

void TileManager::click(int pos)
{
    if(game->move(pos)) {
        QPointF new_empty = tiles[pos]->pos();
        tiles[empty_pos()]->setPos(new_empty);
    }
}

int TileManager::empty_pos()
{
    for(int i=0; i < tiles.size(); ++i) {
        if(tiles[i]->get_id() == 0)
                return i;
    }
}
