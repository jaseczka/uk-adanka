#ifndef BOARDGENERATOR_HPP
#define BOARDGENERATOR_HPP

#include "board.hpp"

class BoardGenerator
{
public:
    BoardGenerator();
    //losowa, układalna permutacja planszy
    Board random();
    //plansza wygenerowana n losowymi ruchami
    Board random(int n);
private:
    //czy wylosowana plansza jest układalna (sprawdza parzystość permutacji)
    bool proper(std::vector<int> board) const;
};

#endif // BOARDGENERATOR_HPP
