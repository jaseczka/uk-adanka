#ifndef TILEMANAGER_HPP
#define TILEMANAGER_HPP

#include <QGraphicsScene>
#include "game.hpp"
#include "tile.hpp"

class TileManager:public QGraphicsScene
{
    Q_OBJECT
    QVector<Tile*> tiles;
    Game* game;
public:
    TileManager(QObject *parent = NULL, Game* game = NULL);
    void setup();
    void redraw();

private:
    int empty_pos();

public slots:
    void click(int pos);
};

#endif // TILEMANAGER_HPP
