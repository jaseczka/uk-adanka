#include "game.hpp"
#include "boardgenerator.hpp"
#include "solver.hpp"

#include <iostream>

Game::Game()
{
    BoardGenerator bg = BoardGenerator();
	board = bg.random();
    start = board;
    history = new std::vector<Board::direction>();
}

Game::Game(int n) {
    BoardGenerator bg = BoardGenerator();
    board = bg.random(n);
    start = board;
    history = new std::vector<Board::direction>();
}

bool Game::move(const int index)
{
    if (board.move(index)) {
        int diff = board.index(board.at(index)) - index;
        switch (diff) {
        case Board::ROW_LENGTH:
            history->push_back(Board::DOWN);
            break;
        case -Board::ROW_LENGTH:
            history->push_back(Board::UP);
            break;
        case 1:
            history->push_back(Board::RIGHT);
            break;
        case -1:
            history->push_back(Board::LEFT);
            break;
        default:
            break;
        }
        return true;
    }
    return false;
}

bool Game::move_empty(const Board::direction d)
{
    if(board.move_empty(d)) {
        history->push_back(d);
        return true;
    }
    return false;
}

int Game::at(const int index)
{
    return board.at(index);
}

std::vector<Board::direction> *Game::get_history()
{
    return history;
}

Board Game::get_board() const
{
    return board;
}

Game::~Game()
{
    delete history;
}

bool Game::solved() const {
    return board.ordered();
}

std::deque<Board::direction> *Game::solve() const {
    std::cout << "solver::solve" << std::endl;
    Solver s;
    return s.solve(start);
}


void Game::undo()
{
    if (!history->empty()) {
        board.move_empty(history->back());
        history->pop_back();
    }
}

void Game::reset()
{
    board = start;
    history->clear();
}


