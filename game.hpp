#ifndef GAME_HPP
#define GAME_HPP

#include "board.hpp"
#include <deque>
#include <vector>

class Game
{
public:
    //gra z losową planszą
    Game();
    //gra z planszą generowaną n ruchami
    Game(int n);
    //przesuwa płytkę z danej pozycji na puste miejsce,
    //jeśli płytka jest obok pustego miejsa
    //zwraca true jeśli się udało
    bool move(int index);
    bool move_empty(Board::direction d);
    int at(int index);
    std::vector<Board::direction>* get_history();
    //cofa ruch
    void undo();
    //ustawia planszę do początkowej pozycji
    void reset();
    //zwraca sekwencję ruchów prowadzącą do rozwiązania
    //planszy startowej (nie bieżącej)
    std::deque<Board::direction>* solve() const;
    bool solved() const;
    Board get_board() const;
    virtual ~Game();
private:
    Board board;
    Board start;
    std::vector<Board::direction>* history;
};

#endif // GAME_HPP
