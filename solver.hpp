#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <vector>
#include <queue>
#include <set>

#include "board.hpp"
#include "astarnode.hpp"

class MyComparison
{
  bool reverse;
public:
  MyComparison(const bool& revparam=false)
    {reverse=revparam;}
  bool operator() ( AStarNode* & lhs,  AStarNode* & rhs) const
  {
    if (reverse) return ( (*lhs)> (*rhs));
    else return ( (*lhs) < (*rhs) );
  }
};

typedef std::priority_queue<AStarNode*, std::vector<AStarNode*>, MyComparison> AStarQueue;

class Solver
{
public:
    Solver();
    std::deque<Board::direction> *solve(const Board& board);
    //odległość planszy od rozwiązania (suma odległości poszczególnych pól w metryce miejskiej
    int distance(const Board& board);
private:
    void push_one(AStarQueue& q, std::set<AStarNode*>& opened,
                  std::set<AStarNode*>& closed, const AStarNode* node, const Board::direction d);
    void push_neighbours(AStarQueue& q, std::set<AStarNode*>& opened,
                         std::set<AStarNode*>& closed, const AStarNode* node);
};

#endif // SOLVER_HPP
