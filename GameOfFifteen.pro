#-------------------------------------------------
#
# Project created by QtCreator 2013-04-22T23:52:40
#
#-------------------------------------------------

QT       += core gui

TARGET = GameOfFifteen
TEMPLATE = app
#QMAKE_CXX = g++-4.7
QMAKE_CXXFLAGS += -std=c++11


SOURCES += main.cpp\
        mainwindow.cpp \
	board.cpp \
	game.cpp \
	boardgenerator.cpp \
	solver.cpp \
#	movessequence.cpp \
    graphicsview.cpp \
    tile.cpp \
    tilemanager.cpp \
    astarnode.cpp

HEADERS  += mainwindow.h \
	board.hpp \
	game.hpp \
	boardgenerator.hpp \
	solver.hpp \
#	movessequence.hpp \
    graphicsview.hpp \
    tile.hpp \
    tilemanager.hpp \
    astarnode.hpp

FORMS    += mainwindow.ui
