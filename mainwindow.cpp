#include <QDebug>
#include <QKeyEvent>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "graphicsview.hpp"
#include "tile.hpp"
#include "tilemanager.hpp"

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    tm = new TileManager();
    QGraphicsView* view = new QGraphicsView(tm);
    setCentralWidget(view);

    resize(450,450);


    view->installEventFilter(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        keyPressEvent(static_cast<QKeyEvent *>(event));
        return true;
    } else {
        return QObject::eventFilter(obj, event);
    }
}


void MainWindow::keyPressEvent(QKeyEvent * event)
{
    if(event->key() == Qt::Key_Up)
        qDebug() << "Gora";
    qDebug() << "Key press" << event->key() ;
}
