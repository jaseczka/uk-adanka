#ifndef GRAPHICSVIEW_HPP
#define GRAPHICSVIEW_HPP

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    GraphicsView(QGraphicsScene *scene, QWidget *parent = NULL);

protected:
    virtual void resizeEvent(QResizeEvent *event);
};

#endif // GRAPHICSVIEW_HPP
